package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String home() {
        return "API REST Tech U Ana v1.0.0";
    }

    // GET a todos los productos del recurso productos y se le llama colección
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        //return "Listado de productos Ana";
        return productoService.getProductos();
    }

    // GET a un unico producto por id que ademas lo muestra en pantalla y se le llama instancia
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoById(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado al buscarlo", HttpStatus.NOT_FOUND);
        }
        //return ResponseEntity.ok(pr); es similar a la siguiente sentencia
        return new ResponseEntity(pr,HttpStatus.OK);
    }

    // POST a recurso productos para crear un producto
    @PostMapping("/productos")
    public ResponseEntity<String> postProducto(@RequestBody ProductoModel newProduct) {
        productoService.addProducto(newProduct);
        return new ResponseEntity<>("Producto creado ok",HttpStatus.CREATED);
    }

    // PUt a productos para actualizar
    @PutMapping("/productos/{id}")
    public ResponseEntity putProductoById(@PathVariable String id,
                                  @RequestBody ProductoModel productoModel) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado al modificarlo", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductobyId(Integer.parseInt(id), productoModel);
        return new ResponseEntity<> ("Producto actualizado ok", HttpStatus.OK);

    }
    // Delete para borrar un producto
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductoById(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado al borrar",HttpStatus.NOT_FOUND);
        }
        productoService.removeProductoById(Integer.parseInt(id));
        return new ResponseEntity<>( HttpStatus.NO_CONTENT);

    }

    // Patch para modificar un producto
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id,
                                    @RequestBody ProductoPrecioModel productoPrecioModel ) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado al patch", HttpStatus.NOT_FOUND);
        }

        productoService.updateProductoPreciobyId(id, productoPrecioModel.getPrecio());
        return new ResponseEntity<>("Producto actualizado ok", HttpStatus.OK);


    }

    // GET de usuarios de un producto y sería sobre un subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity ("Producto no encontrado en usuarios", HttpStatus.NOT_FOUND);
       }
        if (pr.getUsers()!=null) {
            return ResponseEntity.ok(pr.getUsers()); }
        else {
             return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

}
